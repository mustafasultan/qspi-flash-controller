# QSPI Flash Controller



## Introduction

A Flash Memory Controller written in VHDL targeting the Micron Serial NOR Flash Memory (MT25QL128ABA)

## Features

- able to configure the flash memory inner registers
- able to write data to the flash memory in single mode
- able to write data to the flash memory in quad mode
- able to read data from the flash memory
- able to erase the flash memory