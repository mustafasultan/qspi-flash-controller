library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;


entity Flash_Memory_Top is
    -- Clock and Reset from the world/testbench
    clock_in                : in std_logic; -- 100 Mhz
    reset_in                : in std_logic;

    -- Flash Memory Ports
    clk_flash               : out std_logic;
    cs_flash                : out std_logic;
    dq                      : inout std_logic_vector(3 downto 0);

    -- User/PC/TB resgisters file ports
    data_input              : in std_logic_vector(31 downto 0);
    data_output             : out std_logic_vector(31 downto 0);
    address                 : in std_logic_vector(7 downto 0);
    wr                      : in std_logic;
    rd                      : in std_logic
end entity;

architecture Flash_Mem_Top_Arch of Flash_Memory_Top is
    -- Components
    component QSPI_Controller
        port(
            clock	         : in  std_logic; -- 100 Mhz
            reset            : in  std_logic;
            
            qspi_rst         : out  std_logic;
            qspi_dq_in       : in   std_logic_vector(3 downto 0);
            qspi_dq_out      : out  std_logic_vector(3 downto 0);
            qspi_out_en      : out  std_logic;
            qspi_clk_out     : out  std_logic; -- 25MHZ
            qspi_cs          : out  std_logic;
            qspi_init_state  : out  std_logic; -- '1' - init mode, '0'-quad mode
            qspi_dummy_cycle : out std_logic; -- '1' dummy cycles, '0'- no dummy

            
            data_write       : in  std_logic_vector(31 downto 0);
            data_read        : out std_logic_vector(31 downto 0);
            address_data     : in  std_logic_vector(31 downto 0);
            command_data     : in  std_logic_vector(31 downto 0);
            send_data        : in  std_logic_vector(31 downto 0)
                                            
        );                                    
    end component;

    component Registers
        port(
            clock	      :in std_logic;
            reset         :in std_logic;
            
                --User/PC/TB registers file ports
            data_input    :in  std_logic_vector(31 downto 0);
            data_output   :out std_logic_vector(31 downto 0);
            address       :in  std_logic_vector(7  downto 0);
            write_en      :in  std_logic;
            read_en       :in  std_logic;
            
            data_write     :out std_logic_vector(31 downto 0);
            data_read      :in  std_logic_vector(31 downto 0);
            address_data   :out std_logic_vector(31 downto 0);
            command_data   :out std_logic_vector(31 downto 0);
            send_data      :out std_logic_vector(31 downto 0)
                                            
        );                                    
    end component;

    -- QSPI Flash signals
    signal cs_flash_sig  		:std_logic;
    signal qspi_dq_in_sig       :std_logic_vector(3 downto 0);
    signal qspi_dq_out_sig      :std_logic_vector(3 downto 0);
    signal qspi_out_en_sig      :std_logic;
    signal qspi_init_state_sig  :std_logic; -- '1' - init mode, '0'-quad mode
    signal qspi_dummy_cycle_sig :std_logic; --'1' dummy cycles, '0'- no dummy

    -- Register File signals
    signal data_write_sig       :std_logic_vector(31 downto 0);
    signal data_read_sig        :std_logic_vector(31 downto 0);
    signal address_data_sig     :std_logic_vector(31 downto 0);
    signal command_data_sig     :std_logic_vector(31 downto 0);
    signal send_data_sig        :std_logic_vector(31 downto 0);
                    

    begin
        cs_flash <= cs_flash_sig;

        DQ(3 downto 0) <= qspi_dq_out_sig when ((qspi_out_en_sig = '1') and (cs_flash_sig = '0') and (qspi_init_state_sig = '0') and (qspi_dummy_cycle_sig = '0')) else "1ZZ" & qspi_dq_out_sig(0) 
                        when ((qspi_out_en_sig = '1') and (cs_flash_sig = '0') and (qspi_init_state_sig = '1') and (qspi_dummy_cycle_sig = '0')) else "ZZZZ";    
        qspi_dq_in_sig<= DQ(3 downto 0) when ((qspi_out_en_sig = '0') and (cs_flash_sig = '0')) else "0000";


        QSPI_Controller_top : QSPI_Controller
            port map(
                clock	         => clock_in,
                reset            => reset_in,

                qspi_rst         => RESET_FLASH,
                qspi_dq_in       => qspi_dq_in_sig,
                qspi_dq_out      => qspi_dq_out_sig,
                qspi_out_en      => qspi_out_en_sig,
                qspi_clk_out     => CLK_FLASH,
                qspi_cs          => cs_flash_sig,
                qspi_init_state  => qspi_init_state_sig,
                qspi_dummy_cycle => qspi_dummy_cycle_sig,


                data_write       => data_write_sig  ,
                data_read        => data_read_sig   ,
                address_data     => address_data_sig,
                command_data     => command_data_sig,
                send_data        => send_data_sig    
                                                
        );                                    

        Registers_top : Registers
            port map(
                clock	      => CLOCK_IN,
                reset         => RESET_IN,
                
                - -User/PC/TB registers file ports
                data_input     => DATA_INPUT,
                data_output    => DATA_OUTUT,
                address        => ADDRESS   ,
                write_en       => WRITE_EN  ,
                read_en        => READ_EN   ,

                data_write     => data_write_sig  ,
                data_read      => data_read_sig   ,
                address_data   => address_data_sig,
                command_data   => command_data_sig,
                send_data      => send_data_sig   
                                                
        );                                    

end Flash_Mem_Top_Arch;