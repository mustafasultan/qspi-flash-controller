library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity Registers is
    port(
        clock       : in std_logic;
        reset       : in std_logic;

        -- User/PC/TB registers file ports
        data_input  : in std_logic_vector(31 downto 0);
        data_output : out std_logic_vector(31 downto 0);
        address     : in std_logic_vector(7 downto 0);
        wr          : in std_logic; -- Write enable
        rd          : in std_logic  -- Read enable

        data_wr     : out std_logic_vector(31 downto 0);
        data_rd     : in std_logic_vector(31 downto 0);
        addr_data   : out std_logic_vector(31 downto 0);
        cmd_data    : out std_logic_vector(31 downto 0);
        send_data   : out std_logic_vector(31 downto 0)
    );
end Registers;

architecture Registers_Arch of Registers is 

-- Signals
signal  data_wr_sig     : std_logic_vector(31 downto 0);
signal  data_rd_sig     : std_logic_vector(31 downto 0);
signal  addr_data_sig   : std_logic_vector(31 downto 0);
signal  cmd_data_sig    : std_logic_vector(31 downto 0);
signal  send_data_sig   : std_logic_vector(31 downto 0);

begin
data_wr <= data_wr_sig;
data_rd <= data_rd_sig ;
addr_data <= addr_data_sig;
cmd_data <= cmd_data_sig;
send_data <= send_data_sig;

process(reset, clock, rd, data_wr_sig, data_rd_sig, addr_data_sig, cmd_data_sig, send_data_sig) -- Read registers
begin
    if(reset = '0') then
        data_output <= (others => '0');
    elsif(rising_edge(clock)) then
        if(rd = '1') then
            case address is
                when x"00" => data_output <= data_wr_sig;
                when x"04" => data_output <= data_rd_sig;
                when x"08" => data_output <= addr_data_sig;
                when x"0C" => data_output <= cmd_data_sig;
                when x"10" => data_output <= send_data_sig;

                when others => data_output <= x"12345678"; -- Debug magic number
            end case;
        end if;
    end if;
end process;

process(reset, clock, wr) -- Write registers
begin
    if(reset = '0') then
        data_wr <= (others => '0');
        addr_data <= (others => '0');
        cmd_data <= (others => '0');
        send_data <= (others => '0');
    elsif(rising_edge(clock)) then
        if(wr = '1') then -- Is write enabled?
            case address is
                when x"00" => data_wr_sig <= data_input;
                when x"08" => addr_data_sig <= data_input;
                when x"0C" => cmd_data_sig <= data_input;
                when x"10" => send_data_sig <= data_input;

                when others => null;
            end case;
        end if;
    end if;
end process;
end Registers_Arch;
