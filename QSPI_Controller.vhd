library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

entity QSPI_Controller is
    port(
        clock               : in std_logic; -- Global Clock - 100 Mhz 
        reset               : in std_logic;
        
        qspi_rst            : out std_logic;
        qspi_dq_in          : in std_logic_vector(3 downto 0);
        qspi_dq_out         : out std_logic_vector(3 downto 0);
        qspi_out_en         : out std_logic; -- Write enable
        qspi_clk_out        : out std_logic; -- 25 Mhz for STR/DTR
        qspi_cs             : out std_logic;
        qspi_init_state     : out std_logic; -- '1' - Init Mode, '0' - Quad Mode
        qspi_dummy_cycle    : out std_logic; -- '1' - Dummy cycles active, '0' - No dummy cycles
        
        data_wr             : in std_logic_vector(31 downto 0);
        data_rd             : out std_logic_vector(31 downto 0);
        addr_data           : in std_logic_vector(31 downto 0);
        cmd_data            : in std_logic_vector(31 downto 0);
        send_data           : in std_logic_vector(31 downto 0)
    );
end QSPI_Controller;

architecture QSPI_Controller_Arch of QSPI_Controller is 

-- Signals
signal qspi_clock                   : std_logic;
signal qspi_clock_cnt               : std_logic_vector(1 downto 0); -- Count to 2
signal clock_en_falling_edge        : std_logic;
signal clock_en_cnt_falling_edge    : std_logic_vector(2 downto 0); -- Count to 3
signal clock_en_rising_edge         : std_logic;
signal clock_en_cnt_rising_edge     : std_logic_vector(2 downto 0); -- Count to 3

-- Data Read Signals
signal qspi_dq_in_sig               : std_logic_vector(31 downto 0);
signal data_in_counter              : std_logic_vector(3 downto 0); -- Count to 8

-- Quad Command Signals
signal init_cmd                     : std_logic;
signal strt_cmd                     : std_logic;
signal done_cmd                     : std_logic;
signal cmd_data_sig                 : std_logic_vector(7 downto 0);
signal cmd_counter                  : std_logic_vector(1 downto 0);
signal qspi_dq_cmd_out              : std_logic_vector(3 downto 0);

-- Quad Address Signals
signal init_addr                    : std_logic;
signal strt_addr                    : std_logic;
signal done_addr                    : std_logic;
signal addr_data_sig                : std_logic_vector(23 downto 0); -- 3-byte address
signal addr_counter                 : std_logic_vector(2 downto 0); -- Count to 6
signal qspi_dq_addr_out             : std_logic_vector(3 downto 0);

-- Quad Data Signals
signal init_data                    : std_logic;
signal strt_data                    : std_logic;
signal done_data                    : std_logic;
signal data_out_sig                 : std_logic_vector(31 downto 0); 
signal data_counter                 : std_logic_vector(3 downto 0); -- Count to 8
signal qspi_dq_data_out             : std_logic_vector(3 downto 0);

-- Quad Non-volatile Data Signals
signal init_nonvol                  : std_logic;
signal strt_nonvol                  : std_logic;
signal done_nonvol                  : std_logic;
signal nonvol_out_sig               : std_logic_vector(15 downto 0); 
signal nonvol_counter               : std_logic_vector(2 downto 0); -- Count to 4
signal qspi_dq_nonvol_out           : std_logic_vector(3 downto 0);

-- Main State Machine states
type mainStateFsmTypes is (init, rdCmdDataR1, rdStsReg, fastRd, wrEnable, rdCmdDataR2, wrDataToQspi, wr16bitsCmd, wr8bitsCmd, sctrErase);

-- Initialization State Machine states
type initStateFsmTypes is(init, sendCmdWe, waitBfrNxtCmd, sndCmdNonVol, sndSnglData, done);

-- Read Status Register State Machine states
type readStatusFsmTypes is (init, sndQCmdProcess, rdDataProcess, wrDataToRdReg);
signal init_read_status             : std_logic;

-- Fast Read State Machine states
type fastRdFsmTypes is (init, initSndQCmdProcess, initSndQAddrProcess, waitDmmyCycles
                            initRdDataProcess, wrDataToRdReg);
signal dummy_cycles_counter         : std_logic_vector(3 downto 0); -- 10 Dummy Cycle Counter
signal dummy_cycles_done            : std_logic;

-- Write Data to QSPI State Machine states
type wrDataToQspiStates is (init, initSndQCmdProcess, initSndQAddrProcess, initSndQDataProcess);

-- Write 16-bits to QSPI State Machine states
type wr16bitsCmdStates is (init, initSndQCmdProcess, initSendQCmdProcess);

-- Write 8-bits to QSPI State Machine states
type wr8bitsCmdStates is (init, initSndQCmdProcess, initSndQDataProcess);
signal init_8bit_data    		    : std_logic;

-- Sector Erase State Machine states
type sctrEraseStates is (init, initSndQCmdProcess, initSndQAddrProcess);

signal wait_counter                 : std_logic_vector(7 downto 0);
signal cmd_we                       : std_logic_vector(7 downto 0);
signal counter_data                 : std_logic_vector(7 downto 0);
signal qspi_single_init             : std_logic;
signal nonvol_reg_cmd               : std_logic_vector(7 downto 0);
signal nonvol_reg_single            : std_logic_vector(15 downto 0);
signal init_state_done              : std_logic;

begin
    qspi_clock_out <= qspi_clock;

    -- Falling Edge Clock Enable process
    process(clock, reset) is
    begin
        if (reset = '0') then
            clock_en_falling_edge <= '0';
            clock_en_cnt_falling_edge <= "000";
        elsif(falling_edge(clock)) then 
            if (clock_en_cnt_falling_edge < "100") then
                clock_en_cnt_falling_edge <= clock_en_cnt_falling_edge + '1';
                clock_en_falling_edge <= '0';
            else
                clock_en_cnt_falling_edge <= "000";
                clock_en_falling_edge <= '1';
            end if;
        end if;
    end process;

    -- Rising Edge Clock Enable process
    process (clock, reset) is
    begin
        if (reset = '0') then
            clock_en_rising_edge <= '0';
            clock_en_cnt_rising_edge <= "000";
        elsif(rising_edge(clock)) then 
            if (clock_en_cnt_rising_edge < "100") then
                clock_en_cnt_rising_edge <= clock_en_cnt_rising_edge +'1';
                clock_en_rising_edge <= '0';
            else
                clock_en_cnt_rising_edge <= "000";
                clock_en_rising_edge <= '1';
            end if;
        end if;
    end process;

    -- 25 MHz QSPI Clock process
    process(clock, reset) is
    begin
        if (reset = '0') then
            qspi_clock <= '0';
            qspi_clock_cnt <= "00";
        elsif(falling_edge(clock)) then 
            if (qspi_clock_cnt < "10") then
                qspi_clock_cnt <= qspi_clock_cnt + '1';
                qspi_clock <= '0';
            elsif (qspi_clock_cnt = "10" and qspi_clock_cnt = "11") then
                qspi_clock_cnt <= qspi_clock_cnt + '1';
                qspi_clock <= '1';
            else
                qspi_clock <= qspi_clock;
                qspi_clock_cnt <= qspi_clock_cnt;
            end if;
        end if;
    end process;

    -- Read Data Process
    process(clock, reset) is
    begin
        if (reset = '0') then
            qspi_dq_in_sig <= (others => '0');
            data_in_counter <= (others => '0');
        elsif(rising_edge(clock)) then 
            if (clock_en_rising_edge = '1') then
                if (dummy_cycles_done = '1' and data_in_counter < x"8") -- Is dummy cycles done?
                    data_in_counter <= data_in_counter + '1';
                    qspi_dq_in_sig <= data_dq_in;
                    qspi_dq_in_sig <= qspi_dq_in_sig(27 downto 0) & qspi_dq_in_sig(31 downto 28);
                else
                    data_in_counter <= (others => '0');
                    qspi_dq_in_sig <= qspi_dq_in_sig;
                end if;
            end if;
        end if;
    end process;

    -- Send Quad command Process
    process(clock, reset) is
    begin
        if (reset = '0') then
            cmd_data_sig <= x"00";
            cmd_counter <= "00"; -- 2-bit counter
            strt_cmd <= '0';
            done_cmd <= '0';
            qspi_dq_cmd_out <= (others => '0');
        elsif(falling_edge(clock)) then 
            if (clock_en_falling_edge = '1') then
                if (init_cmd = '1') then
                    if (cmd_counter = "00") then
                        cmd_data_sig <= cmd_data(7 downto 0);
                        qspi_dq_cmd_out <= (others => '0');
                        strt_cmd <= '1';
                        done_cmd <= '0';
                        cmd_counter <= cmd_counter + '1';
                    elsif (cmd_counter = "01") then
                        qspi_dq_cmd_out <= cmd_data_sig(7 downto 4); -- MSB
                        strt_cmd <= '1';
                        done_cmd <= '0';
                        cmd_counter <= cmd_counter + '1';
                    elsif (cmd_counter = "10") then
                        qspi_dq_cmd_out <= cmd_data_sig(3 downto 0); -- LSB
                        strt_cmd <= '0';
                        done_cmd <= '1';
                        cmd_counter <= cmd_counter + '1';
                    else
                        cmd_data_sig <= cmd_data_sig;  
                        qspi_dq_cmd_out <= qspi_dq_cmd_out;
                        cmd_counter <= cmd_counter;
                        strt_cmd <= strt_cmd;
                        done_cmd <= done_cmd;
                    end if;
                end if;
            else
                cmd_data_sig <= cmd_data_sig;  
                qspi_dq_cmd_out <= qspi_dq_cmd_out;
                cmd_counter <= "00";
                strt_cmd <= '0';
                done_cmd <= '0';
            end if;
        end if;
    end process; 

    -- Send Quad address Process
    process(clock, reset) is
    begin
        if (reset = '0') then
            addr_data_sig <= x"000000"; -- 24-bit address
            addr_counter <= "000"; -- 3-bit counter
            strt_addr <= '0';
            done_addr <= '0';
            qspi_dq_addr_out <= (others => '0');
        elsif(falling_edge(clock)) then 
            if (clock_en_falling_edge = '1') then
                if (init_addr = '1') then
                    addr_counter <= addr_counter + '1';
                    if (addr_counter = "000") then
                        addr_data_sig <= addr_data(23 downto 0);
                        qspi_dq_addr_out <= (others => '0');
                        strt_addr <= '1';
                        done_addr <= '0';
                    elsif (addr_counter => "001" and addr_counter =< "110") then
                        qspi_dq_addr_out <= addr_data_sig(23 downto 20); -- MSB of Address
                        addr_data_sig <= addr_data_sig(19 downto 0) & addr_data_sig(23 downto 20); -- Rotate 4 bits to the left
                        
                        if (addr_counter = "110") then
                            strt_addr <= '0';
                            done_addr <= '1';
                        else
                            strt_addr <= '1';
                            done_addr <= '0';
                        end if;
                    else
                        addr_data_sig <= addr_data_sig;  
                        qspi_dq_addr_out <= qspi_dq_addr_out;
                        addr_counter <= addr_counter;
                        strt_addr <= strt_addr;
                        done_addr <= done_addr;
                    end if;
                end if;
            else
                addr_data_sig <= addr_data_sig;  
                qspi_dq_addr_out <= qspi_dq_addr_out;
                addr_counter <= "000";
                strt_addr <= '0';
                done_addr <= '0';
            end if;
        end if;
    end process;

    -- Send Quad data Process
    process(clock, reset) is
    begin
        if (reset = '0') then
            data_out_sig <= x"00000000"; -- 32-bit address
            data_counter <= "0000"; -- 4-bit counter
            strt_data <= '0';
            done_data <= '0';
            qspi_dq_data_out <= (others => '0');
        elsif(falling_edge(clock)) then 
            if (clock_en_falling_edge = '1') then
                if (init_addr = '1') then
                    data_counter <= data_counter + '1';
                    if (data_counter = "0000") then
                        data_out_sig <= data_wr(31 downto 0);
                        qspi_dq_data_out <= (others => '0');
                        strt_data <= '1';
                        done_data <= '0';
                    elsif (data_counter => "0001" and data_counter =< "1000") then
                        qspi_dq_data_out <= data_out_sig(31 downto 28); -- MSB of Address
                        data_out_sig <= data_out_sig(27 downto 0) & data_out_sig(31 downto 28); -- Rotate 4 bits to the left
                        
                        if (data_counter = "1000") then
                            strt_data <= '0';
                            done_data <= '1';
                        else
                            strt_data <= '1';
                            done_data <= '0';
                        end if;
                    else
                        data_out_sig <= data_out_sig;  
                        qspi_dq_data_out <= qspi_dq_data_out;
                        data_counter <= data_counter;
                        strt_data <= strt_data;
                        done_data <= done_data;
                    end if;
                end if;
            else
                addr_data_sig <= addr_data_sig;  
                qspi_dq_data_out <= qspi_dq_data_out;
                data_counter <= "0000";
                strt_data <= '0';
                done_data <= '0';
            end if;
        end if;
    end process;      

    -- Send Quad non-volatile data Process (16-bit)
    process(clock, reset) is
    begin
        if (reset = '0') then
            nonvol_out_sig <= x"0000"; -- 16-bit non-volatile data register
            nonvol_counter <= "000"; -- 3-bit counter
            strt_strt_nonvoldata <= '0';
            done_nonvol <= '0';
            qspi_dq_nonvol_out <= (others => '0');
        elsif(falling_edge(clock)) then 
            if (clock_en_falling_edge = '1') then
                if (init_nonvol = '1') then
                    nonvol_counter <= nonvol_counter + '1';
                    if (nonvol_counter = "000") then
                        nonvol_out_sig <= data_wr(15 downto 0);
                        qspi_dq_nonvol_out <= (others => '0');
                        strt_nonvol <= '1';
                        done_nonvol <= '0';
                    elsif (nonvol_counter => "001" and nonvol_counter =< "100") then
                        qspi_dq_nonvol_out <= nonvol_out_sig(15 downto 12); -- MSB of Address
                        nonvol_out_sig <= nonvol_out_sig(11 downto 0) & nonvol_out_sig(15 downto 12); -- Rotate 4 bits to the left
                        
                        if (nonvol_counter = "100") then
                            strt_nonvol <= '0';
                            done_nonvol <= '1';
                        else
                            strt_nonvol <= '1';
                            strt_nonvol <= '0';
                        end if;
                    else
                        nonvol_out_sig <= nonvol_out_sig;  
                        qspi_dq_nonvol_out <= qspi_dq_nonvol_out;
                        nonvol_counter <= nonvol_counter;
                        strt_nonvol <= strt_nonvol;
                        done_nonvol <= done_nonvol;
                    end if;
                end if;
            else
                addr_data_sig <= addr_data_sig;  
                qspi_dq_nonvol_out <= qspi_dq_nonvol_out;
                nonvol_counter <= "000";
                strt_nonvol <= '0';
                done_nonvol <= '0';
            end if;
        end if;
    end process;        

    -- Main State Machine
    process (clock, reset) is
    begin
        if (reset = '0') then
            mainStateFsmTypes <= init;
        else(falling_edge(clock)) then
            if (clock_en_falling_edge = '1') then
                case mainStateFsmTypes is
                    when init =>
                        if (send_data(0) = '1' and init_state_done = '1') then
                            mainStateFsmTypes <= rdCmdDataR1;
                        else
                            mainStateFsmTypes <= init;
                        end if;
                        
                    when rdCmdDataR1 =>
                        if (cmd_data = x"05" or cmd_data = x"70") then
                            mainStateFsmTypes <= rdStsReg;
                        elsif (cmd_data = x"0B") then
                            mainStateFsmTypes <= fastRd;
                        elsif (cmd_data = x"06") then
                            mainStateFsmTypes <= wrEnable;
                        else
                            mainStateFsmTypes <= rdCmdDataR1;
                        end if;
                        
                    when rdStsReg =>
                        if (done_read = '1') then
                            mainStateFsmTypes <= init;
                        else
                            mainStateFsmTypes <= rdStsReg;
                        end if;
                    
                    when fastRd =>
                        if (done_read = '1') then
                            mainStateFsmTypes <= init;
                        else
                            mainStateFsmTypes <= fastRd;
                        end if;
                        
                    when wrEnable =>
                        if (send_data(0) = '1' ) then
                            mainStateFsmTypes <= rdCmdDataR2;
                        else
                            mainStateFsmTypes <= wrEnable;
                        end if;
                    
                    when rdCmdDataR2 =>
                        if (cmd_data = x"02") then
                            mainStateFsmTypes <= wrDataToQspi;
                        elsif (cmd_data = x"B1") then -- Write to non-volatile register
                            mainStateFsmTypes <= wr16bitsCmd;
                        elsif (cmd_data = x"61" or cmd_data = x"81") then -- Write to volatile or enhanched volatile register
                            mainStateFsmTypes <= wr8bitsCmd
                        elsif (cmd_data = x"D8") then
                            mainStateFsmTypes <= sctrErase;
                        else
                            mainStateFsmTypes <= rdCmdDataR2;
                        end if;
                    
                    when wrDataToQspi =>
                        if (done_write = '1') then
                            mainStateFsmTypes <= init;
                        else
                            mainStateFsmTypes <= wrDataToQspi;
                        end if;

                    when wr16bitsCmd =>
                        if (done_write = '1') then
                            mainStateFsmTypes <= init;
                        else
                            mainStateFsmTypes <= wr16bitsCmd;
                        end if;

                    when wr8bitsCmd =>
                        if (done_write = '1') then
                            mainStateFsmTypes <= init;
                        else
                            mainStateFsmTypes <= wr8bitsCmd;
                        end if;

                    when sctrErase =>
                        if (done_write = '1') then
                            mainStateFsmTypes <= init;
                        else
                            mainStateFsmTypes <= sctrErase;
                        end if;
                        
                    when others => mainStateFsmTypes <= init;
                end case;        
            end if;
        end if;
    end process;

    -- Main State Machine
    process (clock, reset) is
    begin
        if (reset = '0') then
            done_read <= '0';
            done_write <= '0';
            init_addr <= '0';
            init_cmd <= '0';
            init_data <= '0';
            init_nonvol <= '0';
            init_8bit_data <= '0'
            qspi_dq_out(3 downto 0) <= "0000";
            qspi_cs <= '1';
            qspi_out_en <= '1'; -- Write Enable
            qspi_init_state <= '1'; -- Init mode
            qspi_dummy_cycle <= '0';
        elsif(falling_edge(clock)) then
            if (clock_en_falling_edge = '1') then
                case mainStateFsmTypes is
                    when init =>
                        done_read <= '0';
                        done_write <= '0';
                        init_addr <= '0';
                        init_cmd <= '0';
                        init_data <= '0';
                        init_nonvol <= '0';
                        qspi_out_en <= '1'; -- Write Enable
                        qspi_dummy_cycle <= '0';

                        if (initStateFsmTypes != init and initStateFsmTypes != done) then
                            qspi_dq_out(3) <= '1';
                            qspi_dq_out(2 downto 1) <= "00";
                            qspi_dq_out(0) <= qspi_single_init;
                            qspi_init_state <= '1'; -- Init Mode 
                            qspi_cs <= '0';
                        else
                            qspi_dq_out <= "0000";
                            qspi_init_state <= '0' -- Quad Mode 
                            qspi_cs <= '1';
                        end if;

                    when rdCmdDataR1 =>
                        qspi_dq_out <= "0000";
                        qspi_cs <= '1';
				        qspi_out_en <= '1'; -- Write Enable

                    when rdStsReg =>
                            if (readStatusFsmTypes = init) then
                                init_cmd = '1';
                                qspi_dq_out <= "0000";
                                qspi_cs <= '1';
                                qspi_out_en <= '1'; -- Write Enable
                            elsif (readStatusFsmTypes = sndQCmdProcess) then
                                init_cmd = '1';
                                qspi_dq_out <= qspi_dq_cmd_out;
                                qspi_cs <= '0';
				                qspi_out_en <= '1'; -- Write Enable
                            elsif (readStatusFsmTypes = rdDataProcess) then
                                init_cmd = '0';
                                qspi_dq_out <= "0000";
                                qspi_cs <= '0';
				                qspi_out_en <= '0'; -- Read Enable
                            elsif (readStatusFsmTypes = wrDataToRdReg) then
                                data_rd <= x"000000" & qspi_dq_in_sig(7 downto 0);
                                qspi_dq_out <= "0000";
                                qspi_cs <= '1';
				                qspi_out_en <= '1'; -- Write Enable
                                read_done <= '1'; 
                            else
                                qspi_dq_out <= "0000";
                                qspi_cs <= '1';
                                qspi_out_en <= '1'; -- Write Enable
                            end if;

                    when fastRd =>
                        if (fastRdFsmTypes = init) then
                            qspi_dq_out <= "0000";
                            qspi_cs <= '1';
                            qspi_out_en <= '1'; -- Write Enable
                            init_cmd = '1';
                        elsif (fastRdFsmTypes = initSndQCmdProcess) then
                            qspi_dq_out <= qspi_dq_cmd_out;
                            qspi_cs <= '0';
                            qspi_out_en <= '1'; -- Write Enable
                            if (cmd_counter = "10") then
                                init_addr <= '1';
                            else
                                init_addr <= '0';
                            end if;
                        elsif (fastRdFsmTypes = initSndQAddrProcess) then
                            qspi_dq_out <= qspi_dq_addr_out;
                            qspi_cs <= '0';
                            spi_out_en <= '1'; -- Write Enable
                            initiate_cmd = '0';                           
                        elsif (fastRdFsmTypes = waitDmmyCycles) then
                            init_addr <= '0';
                            qspi_dq_out <= "0000";
                            qspi_cs <= '0';
                            qspi_out_en <= '1'; -- Write Enable
                            qspi_dummy_cycle <= '1';
                        elsif (fastRdFsmTypes = initRdDataProcess) then
                            qspi_dq_out <= "0000";
                            qspi_cs <= '0';
                            qspi_out_en <= '0'; -- Read Enable
                        elsif (fastRdFsmTypes = wrDataToRdReg) then
                            data_read <= qspi_dq_in_sig;
                            qspi_dq_out <= "0000";
                            qspi_cs <= '1';
                            qspi_out_en <= '1'; -- Write Enable
                            done_read <= '1';
                        else
                            qspi_dq_out <= "0000";
                            qspi_cs <= '1';
                            qspi_out_en <= '1'; -- Write Enable
                        end if;

                    when wrEnable =>
                        if (send_data(0) = '1' and done_cmd = '1') then
                            init_cmd <= '0';
                            qspi_dq_out <= "0000";
                            qspi_cs <= '1';
                        else
                            init_cmd <= '1';
                            qspi_out_en <= '1'; -- Write Enable
                            if (init_cmd = "01" or init_cmd = "10") then
                                qspi_dq_out <= qspi_dq_cmd_out;
                                qspi_cs <= '0';
                            else
                                qspi_dq_out <= "0000";
                                qspi_cs <= '1';
                            end if;
                        end if;

                    when rdCmdDataR2 =>
                        qspi_dq_out <= "0000";
                        qspi_cs <= '1';
                        qspi_out_en <= '1'; -- Write Enable

                    when wrDataToQspi =>
                        if (wrDataToQspiStates = init) then
                            qspi_dq_out <= "0000";
                            qspi_cs <= '1';
                            qspi_out_en <= '1'; -- Write Enable
                            init_cmd = '1';
                            done_write <= '0';
                        elsif (wrDataToQspiStates = initSndQCmdProcess) then
                            qspi_dq_out <= qspi_dq_cmd_out;
                            qspi_cs <= '0';
                            qspi_out_en <= '1'; -- Write Enable
                            if (cmd_counter = "10") then
                                init_addr <= '1';
                            else
                                init_addr <= '0';
                            end if;
                        elsif (wrDataToQspiStates = initSndQAddrProcess) then
                            qspi_dq_out <= qspi_dq_add_out;
                            qspi_cs <= '0';
                            qspi_out_en <= '1'; -- Write Enable
                            init_cmd = '0'
                            if (addr_counter = "110") then
                                init_data <= '1';
                            else
                                init_data <= '0';
                            end if;
                        elsif (wrDataToQspiStates = initSndQDataProcess) then
                            qspi_dq_out <= qspi_dq_data_out;
                            qspi_cs <= '0';
                            qspi_out_en <= '1'; -- Write Enable
                            init_addr <= '0';
                            if (data_counter = "1000") then
                                init_data <= '0';
                                done_write <= '1';
                            else
                                init_data <= '1';
                            end if;
                        else
                            qspi_dq_out <= "0000";
                            qspi_cs <= '1';
                            qspi_out_en <= '1'; -- Write Enable
                        end if;

                    when wr16bitsCmd =>
                        if (wr16bitsCmdStates = init) then
                            qspi_dq_out <= "0000";
                            qspi_cs <= '1';
                            qspi_out_en <= '1'; -- Write Enable
                            init_cmd = '1';
                            done_write <= '0';
                        elsif (wr16bitsCmdStates = initSendQCmdPr) then
                            qspi_dq_out <= qspi_dq_cmd_out;
                            qspi_cs <= '0';
                            qspi_out_en <= '1'; -- Write Enable
                            if (cmd_counter = "10") then
                                init_nonvol <= '1';
                            else
                                init_nonvol <= '0';
                            end if;
                        elsif (wr16bitsCmdStates = initSendQCmdPr) then
                            qspi_dq_out <= qspi_dq_nonvol_out;
                            qspi_cs <= '0';
                            qspi_out_en <= '1'; -- Write Enable
                            init_addr <= '0';
                            if (nonvol_counter = "100") then
                                init_nonvol <= '0';
                                done_write <= '1';
                            else
                                init_nonvol <= '0';
                            end if;
                        else
                            qspi_dq_out <= "0000";
                            qspi_cs <= '1';
                            qspi_out_en <= '1'; -- Write Enable
                        end if;

                    when wr8bitsCmd =>
                        if (wr8bitsCmdStates = init) then
                            qspi_dq_out <= "0000";
                            qspi_cs <= '1';
                            qspi_out_en <= '1'; -- Write Enable
                            init_cmd = '1';
                            done_write <= '0';
                        elsif (wr8bitsCmdStates = initSendQCmdPr) then
                            qspi_dq_out <= qspi_dq_cmd_out;
                            qspi_cs <= '0';
                            qspi_out_en <= '1'; -- Write Enable
                            if (cmd_counter = "10") then
                                init_8bit_data <= '1';
                            else
                                init_8bit_data <= '0';
                            end if;
                        elsif (wr8bitsCmdStates = initSendQCmdPr) then
                            qspi_dq_out <= qspi_dq_nonvol_out;
                            qspi_cs <= '0';
                            qspi_out_en <= '1'; -- Write Enable
                            init_addr <= '0';
                            if (data_counter = "0010") then
                                init_8bit_data <= '0';
                                done_write <= '1';
                            else
                                qspi_cs <= '0';
                                init_8bit_data <= '1';
                            end if;
                        else
                            qspi_dq_out <= "0000";
                            qspi_cs <= '1';
                            qspi_out_en <= '1'; -- Write Enable
                        end if;

                    when sctrErase =>
                        if (sctrEraseStates = init) then
                            qspi_dq_out <= "0000";
                            qspi_cs <= '1';
                            qspi_out_en <= '1'; -- Write Enable
                            init_cmd = '1';
                            done_write <= '0';
                        elsif (sctrEraseStates = initSndQCmdProcess) then
                            qspi_dq_out <= qspi_dq_cmd_out;
                            qspi_cs <= '0';
                            qspi_out_en <= '1'; -- Write Enable
                            if (command_counter = "10") then
                                init_addr <= '1';
                            else
                                init_addr <= '0';
                            end if;
                        elsif (sctrEraseStates = initSndQAddrProcess) then
                            qspi_dq_out <= qspi_dq_add_out;
                            qspi_out_en <= '1'; -- Write Enable
                            init_cmd = '0';
                            if (init_addr = "110") then
                                init_addr <= '0';
                                done_write <= '1';
                                qspi_cs <= '1';
                            else
                                qspi_cs <= '0';
                                init_addr <= '1';
                            end if;
                        else
                            qspi_dq_out <= "0000";
                            qspi_cs <= '1';
                            qspi_out_en <= '1'; -- Write Enable
                        end if;     
  
                    when others => 
                        qspi_dq_out <= "0000";
                        qspi_cs <= '1';
                        qspi_out_en <= '1'; -- Write Enable
                end case;
            end if;
        end if;
    end process;

    -- Initialization State Machine
    process (clock, reset) is
    begin
        if (reset = '0') then
            initStateFsmTypes <= init;
            wait_counter <= x"00";
            counter_data <= x"00";
            qspi_single_init <= '0';
            cmd_we <= x"06"; -- Write enable command
            nonvol_reg_cmd <= x"B1"; -- Write nonvolatile register
            nonvol_reg_single <= x"F7AF"; -- Send AFF7h to the non-volatile register 
            init_state_done <= '0';
        elsif(falling_edge(clock)) then
            if (clock_en_falling_edge = '1') then
                case initStateFsmTypes is
                    when init =>
                        if (wait_counter = x"FF") then
                            wait_counter <= x"00";
                            initStateFsmTypes <= sendCmdWe;
                        else
                            initStateFsmTypes <= init;
                            wait_counter <= wait_counter + '1';
                        end if;
                        
                    when sendCmdWe =>
                        if (counter_data = x"09") then
                            initStateFsmTypes <= waitBfrNxtCmd;
                            counter_data <= x"00";
                            qspi_single_init <= '0';
                        else
                            qspi_single_init <= cmd_we(7);
                            cmd_we <= cmd_we(6 downto 0) & cmd_we(7);
                            counter_data <= counter_data + '1';
                            initStateFsmTypes <= sendCmdWe;
                        end if;
                        
                    when waitBfrNxtCmd =>
                        qspi_single_init <= '0';
                        if (wait_counter = x"B0") then
                            wait_counter <= x"00";
                            initStateFsmTypes <= sndCmdNonVol;
                        else
                        initStateFsmTypes <= waitBfrNxtCmd;
                            wait_counter <= wait_counter + '1';
                        end if;
                        
                    when sndCmdNonVol =>
                        if (counter_data = x"09") then
                            initStateFsmTypes <= sndSnglData;
                            counter_data <= x"00";
                            qspi_single_init <= '0';
                        else -- counter_data < x"09"
                            qspi_single_init <= nonvol_reg_cmd(7);
                            nonvol_reg_cmd <= nonvol_reg_cmd(6 downto 0)& nonvol_reg_cmd(7);
                            counter_data <= counter_data + '1';
                            initStateFsmTypes <= sndCmdNonVol;
                        end if;
                    
                    when sndSnglData =>
                        if (counter_data = x"10") then
                            initStateFsmTypes <= done;
                            counter_data <= x"00";
                            qspi_single_init <= '0';
                        else -- counter_data < x"10"
                            qspi_single_init <= nonvol_reg_single(15);
                            nonvol_reg_single <= nonvol_reg_single(14 downto 0) & nonvol_reg_single(15);
                            counter_data <= counter_data + '1';
                            initStateFsmTypes <= sndSnglData;
                        end if;
                    
                    when done =>
                        initStateFsmTypes <= done;
                        qspi_single_init <= '0';
                        init_state_done <= '1';
                    when others => initStateFsmTypes <= done;
                end case;
            end if;
        end if;
    end process;

    -- Read Status Register State Machine
    process (clock, reset) is
    begin
        if (reset = '0') then
            readStatusFsmTypes <= init;
            init_read_status <= '0';
        elsif(falling_edge(clock)) then
            if (clock_en = '1') then
                case readStatusFsmTypes is
                    when init =>
                        if (mainStateFsmTypes = rdStsReg) then
                            readStatusFsmTypes <= sndQCmdProcess;
                        else
                            readStatusFsmTypes <= init;
                        end if;
                        
                    when sndQCmdProcess =>
                        if (cmd_counter = "10") then
                            readStatusFsmTypes <= rdDataProcess;
                        else
                            readStatusFsmTypes <= sndQCmdProcess;
                        end if;
                    
                    when rdDataProcess =>
                        if (data_in_counter = x"2") then
                            readStatusFsmTypes <= wrDataToRdReg;
                            init_read_status <= '0';
                        else
                            init_read_status <= '1';
                            readStatusFsmTypes <= rdDataProcess;
                        end if;
                    
                    when wrDataToRdReg =>
                        initStateFsmTypes <= init;

                    when others => initStateFsmTypes <= init;
                end case;
            end if;
        end if;
    end process;

    -- Fast Read State Machine
    process (clock, reset) is
    begin
        if (reset = '0') then
            fastRdFsmTypes <= init;
            dummy_cycles_done <= '0';
            dummy_cycles_counter <= "0000";
        elsif(falling_edge(clock)) then
            if (clock_en = '1') then
                case fastRdFsmTypes is
                    when init =>
                        dummy_cycles_done <= '0'; 
                        dummy_cycles_counter <= "0000";
                        if (mainStateFsmTypes = fastRd) then
                            fastRdFsmTypes <= initSndQCmdProcess;
                        else
                            fastRdFsmTypes <= init;
                        end if;
                    
                    when initSndQCmdProcess =>
                        if (cmd_counter = "10") then
                            fastRdFsmTypes <= initSndQAddrProcess;
                        else
                            fastRdFsmTypes <= initSndQCmdProcess;
                        end if;
                    
                    when initSndQAddrProcess =>
                        if (addr_counter = "110") then
                            fastRdFsmTypes <= waitDmmyCycles;
                        else
                            fastRdFsmTypes <= initSndQAddrProcess;
                        end if;
                    
                    when waitDmmyCycles =>
                        dummy_cycles_counter <= dummy_cycles_counter + '1';
                        if (dummy_cycles_counter = "1010") then -- 10 dummy cycles
                            fastRdFsmTypes <= initRdDataProcess;
                        else
                            fastRdFsmTypes <= waitDmmyCycles;
                        end if;
                        
                    when initRdDataProcess =>
                        dummy_cycles_done <= '1';
                        if (data_in_counter = x"8") then
                            fastRdFsmTypes <= wrDataToRdReg;
                        else
                            fastRdFsmTypes <= initRdDataProcess;
                        end if; 
                    
                    when wrDataToRdReg =>
                        data_rd <= qspi_dq_in_sig;
                        fastRdFsmTypes <= init;
    
                    when others => fastRdFsmTypes <= init;
                end case;               
            end if;
        end if;
    end process;

    -- Write Data to QSPI State Machine 
    process (clock, reset) is
    begin
        if (reset = '0') then
            wrDataToQspiStates <= init;
    
        elsif(falling_edge(clock)) then
            if (clock_en = '1') then
                case wrDataToQspiStates is
                    when init =>
                        if (mainStateFsmTypes = wrDataToQspi) then
                            wrDataToQspiStates <= initSndQCmdProcess;
                        else
                            wrDataToQspiStates <= init;
                        end if;
                    
                    when initSndQCmdProcess =>
                        if (cmd_counter = "10") then
                            wrDataToQspiStates <= initSndQAddrProcess;
                        else
                            wrDataToQspiStates <= initSndQCmdProcess;
                        end if;
                    
                    when initSndQAddrProcess =>
                        if (addr_counter = "110") then
                            wrDataToQspiStates <= initSndQDataProcess;
                        else
                            wrDataToQspiStates <= initSndQAddrProcess;
                        end if;
                        
                    when initSndQDataProcess =>
                        if (data_counter = "1000") then
                            wrDataToQspiStates <= init;
                        else
                            wrDataToQspiStates <= initSndQDataProcess;
                        end if;
    
                    when others => wrDataToQspiStates <= init;
                end case;
            end if;
        end if;
    end process;

    --  Write 16-bits to QSPI State Machine
    process (clock, reset) is
    begin
        if (reset = '0') then
            wr16bitsCmdStates <= init;
        elsif(falling_edge(clock)) then
            if (clock_en = '1') then
                case wr16bitsCmdStates is
                    when init =>
                        if (mainStateFsmTypes = wr16bitsCmd) then
                            wr16bitsCmdStates <= initSndQCmdProcess;
                        else
                            wr16bitsCmdStates <= init;
                        end if;
                    
                    when initSndQCmdProcess =>
                        if (cmd_counter = "10") then
                            wr16bitsCmdStates <= initSendQCmdProcess;
                        else
                            wr16bitsCmdStates <= initSndQCmdProcess;
                        end if;
                    
                    when initSendQCmdProcess =>
                        if (nonvol_counter = "100") then
                            wr16bitsCmdStates <= init;
                        else
                            wr16bitsCmdStates <= initSendQCmdProcess;
                        end if;
    
                    when others => wr16bitsCmdStates <= init;
                end case;
            end if;
        end if;
    end process;    

    --  Write 8-bits to QSPI State Machine
    process (clock, reset) is
    begin
        if (reset = '0') then
            wr8bitsCmdStates <= init;
        elsif(falling_edge(clock)) then
            if (clock_en = '1') then
                case wr8bitsCmdStates is
                    when init =>
                        if (mainStateFsmTypes = wr8bitsCmd) then
                            wr8bitsCmdStates <= initSendQCmdPr;
                        else
                            wr8bitsCmdStates <= init;
                        end if;
                    
                    when initSendQCmdPr =>
                        if (cmd_counter = "10") then
                            wr8bitsCmdStates <= initSndQDataProcess;
                        else
                            wr8bitsCmdStates <= initSendQCmdPr;
                        end if;
                    
                    when initSndQDataProcess =>
                        if (data_counter = "10") then
                            wr8bitsCmdStates <= init;
                        else
                            wr8bitsCmdStates <= initSndQDataProcess;
                        end if;
    
                    when others => wr8bitsCmdStates <= init;
                end case;
            end if;
        end if;
    end process;    

    -- Sector Erase State Machine
    process (clock, reset) is
    begin
        if (reset = '0') then
            sctrEraseStates <= init;
        elsif(falling_edge(clock)) then
            if (clock_en = '1') then
                case sctrEraseStates is
                    when init =>
                        if (mainStateFsmTypes = sctrErase) then
                            sctrEraseStates <= initSndQCmdProcess;
                        else
                            sctrEraseStates <= init;
                        end if;
                    
                    when initSndQCmdProcess =>
                        if (command_counter = "10") then
                            sctrEraseStates <= initSndQAddrProcess;
                        else
                            sctrEraseStates <= initSndQCmdProcess;
                        end if;
                    
                    when initSndQAddrProcess =>
                        if (address_counter = "110") then
                            sctrEraseStates <= init;
                        else
                            sctrEraseStates <= initSndQAddrProcess;
                        end if;
    
                    when others => sctrEraseStates <= init;
                end case;
            end if;
        end if;
    end process;
end QSPI_Controller_Arch;