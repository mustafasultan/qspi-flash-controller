library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity Flash_Memory_TB is 
end entity;

begin 

architecture Flash_Memory_TB_Arch of Flash_Memory_TB
component Flash_Mem_Top 
    port(
        -- Clock and Reset from the world/testbench
        clock_in : in std_logic; -- 25Mhz
        reset_in : in std_logic;
    
        -- Flash Memory Ports
        clk_flash : out std_logic;
        cs_flash :out std_logic;
        dq : inout std_logic_vector(3 downto 0);
    
        -- User/PC/TB resgisters file ports
        data_input : in std_logic_vector(31 downto 0);
        data_output : out std_logic_vector(31 downto 0);
        address : in std_logic_vector(7 downto 0);
        wr : in std_logic;
        rd : in std_logic
    );
    
end component;

-- Signals 
    -- Clock/Reset from the outside world/testbench
    signal clock_in_sig : std_logic:='0'; -- 25Mhz
    signal reset_in_sig : std_logic:='0';

    -- Flash Memory Ports
    signal clk_flash_sig : std_logic;
    signal cs_flash_sig : std_logic;
    signal dq_sig : std_logic_vector(3 downto 0);

    -- User/PC/TB registers file ports
    signal data_input_sig : std_logic_vector(31 downto 0);
    signal data_output_sig : std_logic_vector(31 downto 0);
    signal address_sig : std_logic_vector(7 downto 0);
    signal wr_sig : std_logic;
    signal rd_sig : std_logic;
-- End of Signals

begin 
Flash_Memory_TB: Flash_Mem_Top 
    port map(
        -- Clock and Reset from the world/testbench
        clock_in => clock_in_sig,
        reset_in => reset_in_sig,

        -- Flash Memory Ports
        clk_flash => clk_flash_sig,
        cs_flash => cs_flash_sig,
        dq => dq_sig,

        -- User/PC/TB resgisters file ports
        data_input => data_input_sig, 
        data_output => data_output_sig,
        address => address_sig, 
        wr => wr_sig, 
        rd => rd_sig
    );

clock_in_sig <= not clock_in_sig after 20 ns; --25MHZ
reset_in_sig <= '0', '1' after 1 us;
    
end Flash_Memory_TB_Arch;